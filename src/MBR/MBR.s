##File: MBR.s 

##Purpose: Since my partition editing tool didn't provide a functional MBR, I figured I'd make a quick and dirty one.  It has been a while, and parted may or may not provide one anyway, but since I have this one, I'll just run with it. 

##Note: As far as I know, this does what any respectable MBR does, such as checking the VBR boot signature, preserving DL, and relocating out of the way of the VBR (well, that is needed just to make it work :P), and printing an error message in case of failure.  



.code16 
.org 0 

##Relocation to 0x2000
#Lets relocate to 0x2000 
########
movw $0x7c00, %si #Source  
movw $0x2000, %di #Destination 
movw $0x200, %cx #512 bytes, right? 
rep movsb #Actual command 
movw $0x0, %ax #Reloading data segments.... 
movw %ax, %ds 
movw %ax, %es 
movw %ax, %fs 
movw %ax, %gs 
movw %ax, %ss 
jmpl $0x0, $main #Long jump to main 
########


main: 
	##MAIN initialization
	########
	movw $0x9000, %sp #Stack setup 
	movb $0x04, %cl #Set counter 
	movw $0x21be, %di  #Set the pointer to the first partition table entry 
	movw $BootSignatureError, %si
	########
	##Partition loop
	########
	.partitioncheckloop: 
		movb (%di), %al #Get the first byte of the entry 
		cmpb $0x80, %al #Is it an active partition? 
		je .partitionfound #If so, we got it! 
		add $0x10, %di #Otherwise, move on to the next entry 
		decb %cl #Decrement counter 
		cmpb $0x0, %cl #Have we looked at all of the entries already? 
		jne .partitioncheckloop #If not, keep going 
		movw $NoBootablePartition, %si 
		jmp PrintError
	##VBR Handling Code
	########
		.partitionfound: 
			##Load the VBR
			########
			add $0x08, %di #Lets get the LBA of the first sector of the partition 
			movl (%di), %eax #Put it in EAX 
			movw $LBALocationLower, %di #Self-explanatory 
			movl %eax, (%di) #Load the lower 32 bits into the packet 
			movw $LBAAddressPacket, %si #Set the address of the data packet 
			movb $0x42, %ah #Function selection
			int $0x13  #Do the interrupt call  
			########
			##Check the BOOT signature 
			movw $0x7dFE, %di  #Load BOOT signature address 
			movw (%di), %ax #Load the signature into AX 
			cmpw $0xAA55, %ax #Compare it to 0xAA55 
			movw $BootSignatureError, %si
			jne PrintError  #If it isn't equal, hang. 
			########
			##Jump to VBR
			movw $0x7c00, %ax #Load the address of the VBR
			jmp %ax #Jump to it
			########
	########


PrintError: 
	lodsb 
	cmp $0, %al 
	je .hang 
	push %si 
	movb $0x0E, %ah 
	int $0x10 
	pop %si 
	jmp PrintError 

########
##Hang loop 
########
	.hang: 
		jmp .hang 
########

NoBootablePartition: 
	.ascii "No bootable partitions found." 
	.byte 0

BootSignatureError: 
	.ascii "VBR boot signature not 0xAA55." 
	.byte 0

.org 200 #This is just to make sure the packet is aligned and not fucking with any of the code prior 
LBAAddressPacket: 
		.byte 0x10 
		.byte 0 
		BlockReadSize: 
			.word 1 
		BufferLocation: 
			.word 0x7c00 
		.word 0 
		LBALocationLower: 
			.long 0 
		LBALocationUpper: 
			.long 0 


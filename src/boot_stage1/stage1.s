##File: stage1.s 

##Purpose: This is the boot sector for the FAT32 BOOT partition. It loads the file boot.bin in the root directory of the partition to 0xC200 and passes control to it. 


##Note: boot.bin should not be bigger than 15873 bytes due to the current segmentation layout in stage1. I don't know if the bios cares or not, but it is best not to risk it. 



.code16 
.org 0 

    
.macro FunctionEntry 
	pusha 
	movw %sp, %bp 
	add $18, %bp 
.endm 

.macro FunctionExit 
	popa 
	ret
.endm 
 
movw $0x0, %ax 
movw %ax, %ds 
movw %ax, %es 
movw %ax, %fs 
movw %ax, %gs 
movw %ax, %ss 
jmpl $0x0, $thestart 

thestart: 
	movw $0x8000, %sp # Lets do a 512 byte stack, eh? 
	#Time to start loading stage two.... 
	movw $0x7c00, %di 
	movb %dl, (%di) 
	movl 44(%di), %ecx  #Get the cluster number of the root directory 
	pushl %ecx #Push the cluster number 
	call SearchForFileEntry #Get the target file cluster number 
	movw $FileClusterNumber, %di  #Self-explanatory 
	movl (%di), %eax #Load the file cluster number into eax 
	movw $0xC200, %si
	pushl %eax #Push the first cluster number 
	call LoadClusterChain #Load the file 
	movw $0xC200, %ax 
	jmp %ax #Transfer control to stage two 
 

LoadBlocks: 
	FunctionEntry  #Basic initialization 
	#movw (%bp), %dx #Load the upper 16 bits of the LBA 
	movl (%bp), %eax #Load the lower 32 bits of the LBA 
	movw $0x7c1c, %di #Self-explanatory 
	addl (%di), %eax #Add the LBA start to the desired address 
	#movw $LBALocationUpper, %di #Self-explanatory 
	#movw %dx, (%di) #Load the upper 16 bits into the packet 
	movw $LBALocationLower, %di #Self-explanatory 
	movl %eax, (%di) #Load the lower 32 bits into the packet 
	movw $BufferLocation, %di #Self-explanatory 
	movw 4(%bp), %dx #Load the buffer location 
	movw %dx, (%di) #Put it into the packet 
	movw $BlockReadSize, %di #Self-explanatory 
	movw 6(%bp), %dx  #Get the block read size 
	movw %dx, (%di) #Put it into the packet 
	movw $LBAAddressPacket, %si #Set the address of the data packet 
	movb $0x42, %ah #Function selection 
	movw $0x7c00, %di 
	movb (%di), %dl #Set the drive number
	int $0x13  #Do the interrupt call 
	FunctionExit #Cleanup and exit  
		
LoadFATSection:   
	FunctionEntry #Basic initialization 
	movw $0x7c0E, %di #The address of the FAT. 
	xor %edx, %edx #Clear EDX 
	movw (%di), %dx #Load the FAT address into EDX 
	movl (%bp), %eax  #Load FAT section
	add %eax, %edx  #Add this to the FAT address 
	pushw $1  #Number of sectors to load 
	pushw $0x8001 #Offloading address 
	pushl %edx #Push the address of the FAT section 
	#pushw $0  #Upper LBA (unused) 
	call LoadBlocks #Load the fat section 
	add $8, %sp  #Stack cleanup 
	FunctionExit #Cleanup and exit 

LoadFATSectionViaCluster: 
	FunctionEntry #Basic initialization 
	movl (%bp), %eax #Get cluster number 
	movl $128, %ebx #Self-explanatory 
	divl %ebx #Divide the cluster number by the number of cluster entries in each sector of the FAT 
	pushl %eax #Push the FAT section number 
	call LoadFATSection #Load it 
	add $4, %sp #Cleanup 
	mov $Remainder, %di #Self-explanatory 
	mov %dx, %ax 
	mov $4, %dx 
	mul %dx 
	movw %ax, (%di) #Load the remainder (useful later) into $Remainder 
	FunctionExit 

LoadCluster: 
	FunctionEntry 
	movw $0x7c00, %di 
	xor %edx, %edx #Assert EDX to zero  
	movw 16(%di), %dx  #Get number of FATs 
 	movl 36(%di), %eax #Get sectors per FAT 
	mull %edx #Put in EAX the number of sectors taken by FATs 
	movw 14(%di), %dx #Get number of reserved sectors 
	add  %edx, %eax #Get the address of the first data sector 
	movl %eax, %ecx #Put the address of the first data sector into ECX
	xor %ebx, %ebx #Clear EBX  
	movl (%bp), %eax #Get the cluster number 
	movb 13(%di), %bl #Get number of sectors per cluster 
	sub $2, %eax #Subtract cluster number by two 
	mull %ebx #multiply the number of sectors per cluster by the cluster number 
	add %eax, %ecx #Get the first sector of the cluster 
	pushw %bx #Push the number of sectors per cluster 	
	movw 4(%bp), %dx #Get the buffer address 
	pushw %dx  #Repush it 
	pushl %ecx #Push the first sector of the cluster 
	#pushw $0 #Upper 16 bits of the LBA (unused) 
	call LoadBlocks 
	add $8, %sp 
	FunctionExit 

LoadClusterChain: 
	FunctionEntry 
	movl (%bp), %ecx #Load the cluster number 
	movw $0x7c00, %di #Set DI to the start of the boot sector 
	movb 13(%di), %al #Get number of sectors per cluster 
	xor %ah, %ah #Clear the higher 8 bits of AX 
	movw $0x200, %dx #Set DX to 512 
	mul %dx  #Multiply AX by DX  
	.ClusterChainLoop:  
		pushw %si #Push the unloading address 
		pushl %ecx #Push the cluster number 
		call LoadCluster #Load the cluster 
		call LoadFATSectionViaCluster #Load the FAT section that has the cluster number in it 
		add $6, %sp #Stack cleanup  
		mov $Remainder, %di #Self-explanatory 
		movw (%di), %di #L oad the remainder into di  
		add $0x8001, %di #Add the base address of the FAT to it... 
		add %ax, %si
		movl (%di), %ecx #Get the next cluster number 
		cmp $0xFF7, %ecx #Check if the next cluster is good.   
		jb .ClusterChainLoop
	.ClusterChainEnd: 
		FunctionExit 

SearchForFileEntry: 
	FunctionEntry 
	movl (%bp), %eax #Load the root directory cluster number 
	movw $0x8401, %si #Set the offloading address
	pushl %eax #Push the root directory cluster number 
	call LoadClusterChain #Load the root directory 
	add $4, %sp #Cleanup 
	movw $0x8401, %si #Initialize the pointer for the root directory entries 
	.searchloop: 
		cmpb $0, (%si) #If the first byte of the entry is zero, there are no more entries 
		.searchhangloop: 
			je .searchhangloop #Quit and cry I guess....  
		movl (%si), %edx 
		cmpl $0x544f4f42, %edx #Compare the first four letters to "BOOT"
		jne .loopend #If not so, move on to the next entry 
		movl 8(%si), %edx #Get the file extension number 
		and $0x00FFFFFF, %edx #Clear the last letter out... 
		cmpl $0x004E4942, %edx #Compare it to "BIN" 
		jne .loopend #If not so, move on to the next entry 
		#If we're still here, we found the file! 
		movw 20(%si), %dx #Get the higher bits of the cluster number 
		shl $16, %edx #Shift it up two bytes so it is in the right spot. 
		movw 26(%si), %dx #Get the lower bits of the cluster number and put it in 
		movw $FileClusterNumber, %di #Self-explanatory 
		movl %edx, (%di) #Stick the cluster number in for later use. 	
		.exit: 
			FunctionExit 
		.loopend: 
			add $32, %si #Shift the pointer to the next entry 
			jmp .searchloop #Restart the loop 

Remainder: 
	.word 0 

FileClusterNumber: 
	.long 0 

	
.org 404 	/*This ensures that the LBA packet and the boot signature are on the very end. The reason I am treating the LBA packet this way is to ensure that it is
			aligned on a 4 byte boundary, and using .balign wasn't going so well :P */ 
	LBAAddressPacket: 
		.byte 0x10 
		.byte 0 
		BlockReadSize: 
			.word 0 
		BufferLocation: 
			.word 0 
		.word 0 
		LBALocationLower: 
			.long 0 
		LBALocationUpper: 
			.long 0 
	.byte 0x55 
	.byte 0xAA

.code16 
.org 0 

.macro DoInterrupt, intn 
	pusha 
	int $\intn   
	popa  
.endm 

.macro FunctionEntry 
	pusha 
	movw %sp, %bp 
	add $18, %bp 
.endm 

.macro FunctionExit 
	popa 
.endm 

init: 
	movw $0, %ax 
	movw %ax, %ds 
	movw %ax, %es 
	movw %ax, %fs 
	movw %ax, %gs 
	movw %ax, %ss 
	jmpl $0, $start  

start: 
	call video_init 
	call SearchForConfigFile 
	call menu_init  
	call MenuManagement
	.permloop:
		jmp .permloop 

menu_init: 
	call video_hidecursor 
	.TitleMessageDisplay:
		movw $video_cursor_column, %si 
		movb $0x1C, %dl 
		movb %dl, (%si) 
		call video_updatecursor 
		pushw $TitleMessage 
		call PrintString 
		add $2, %sp 
	.BootOptionTitleDisplay:
		movw $video_cursor_column, %di 
		movw $video_cursor_row, %si 
		movb $0x04, %dl 
		movb %dl, (%di) 
		movb %dl, (%si) 
		call video_updatecursor 
		pushw $BootOptions 
		call PrintString 
		add $2, %sp 
	.BootOptionListDisplay: 
		call PrintMenu
 	ret 


SearchForConfigFile: 
	FunctionEntry 
	movw $0x8401, %si #Initialize the pointer for the root directory entries 
	.searchloop: 
		cmpb $0, (%si) #If the first byte of the entry is zero, there are no more entries  
		.searchhangloop: 
			je .searchhangloop #Quit and cry I guess....  
		movl (%si), %edx 
		cmpl $0x544f4f42, %edx #Compare the first four letters to "BOOT"  
		jne .loopend #If not so, move on to the next entry 
		movl 8(%si), %edx #Get the file extension number 
		and $0x00FFFFFF, %edx #Clear the last letter out... 
		cmpl $0x00474643, %edx #Compare it to "CFG" 
		jne .loopend #If not so, move on to the next entry 
		#If we're still here, we found the file! 
		movw 20(%si), %dx #Get the higher bits of the cluster number 
		shl $16, %edx #Shift it up two bytes so it is in the right spot. 
		movw 26(%si), %dx #Get the lower bits of the cluster number and put it in 
		pushw $0x6000
		pushl %edx 
		call LoadCluster 
		add $6, %sp 	
		.exit: 
			FunctionExit 
			ret
		.loopend: 
			add $0x20, %si #Shift the pointer to the next entry 
			jmp .searchloop #Restart the loop


menu_currententry: 
	.byte 0 

menu_previousentry: 
	.byte 0 

MenuManagement: 
	pusha 
	movw $EntriesNum, %di 
	movb (%di), %dl 
	movw $menu_previousentry, %di 
	movw $menu_currententry, %si 
	.ManageLoop: 
		call menu_update 
		movw $0, %ax 
		int $0x16 
		cmp $0x48, %ah 
		je .UpArrow 
		cmp $0x50, %ah 
		je .DownArrow 
		cmp $0x1C, %ah 
		je .EnterKey
		jmp .permloop 
		.UpArrow: 
			movb (%si), %al 
			cmp $0, %al 
			je .ManageLoop
			movb %al, (%di) 
			dec %al 
			movb %al, (%si) 
			jmp .ManageLoop
		.DownArrow: 
			movb (%si), %al 
			cmp %dl, %al 
			je .ManageLoop
			movb %al, (%di) 
			inc %al 
			movb %al, (%si) 
			jmp .ManageLoop 
		.EnterKey: 
			xor %ax, %ax 
			movb (%si), %al 
			pushw %ax 
			call ChainLoad  #Never returns 


ChainLoad: 
	FunctionEntry 
	movw (%bp), %ax 
	movw $0x10, %dx 
	mulw %dx 
	movw %ax, %di 
	add $0x21c6, %di 
	movl (%di), %eax 
	movw $0x7c00, %di 
	movb (%di), %dl 
	pushw $1 
	pushw $0x7c00 
	pushl %eax 
	call LoadBlocks_raw 
	movw $0x7c00, %ax 
	jmp %ax 
	

menu_update: 
	pusha 
	pushw $0x1E 
	movb $0x07, %al 
	movb (%di), %ah 
	add $0x07, %ah 
	pushw %ax 
	pushw $0x07 
	call video_changecolor 
	add $0x04, %sp 
	movb (%si), %ah 
	add $0x07, %ah 
	pushw %ax 
	pushw $0xF0 
	call video_changecolor 
	add $0x06, %sp 
	popa 
	ret 

#####################Video Primitives################################## 

PrintChar: 
	FunctionEntry 
	movb (%bp), %al  
	movb $0x0A, %ah 
	movw $0, %bx 
	movw $1, %cx 
	DoInterrupt 0x10 
	call video_advancecursor 
	FunctionExit 
	ret 
	
PrintString: 
	FunctionEntry 
	movw (%bp), %bx  
	.MainLoop: 
		movb (%bx), %al 
		cmp $0x5E, %al  
		je .printdone 
		pushw %ax 
		call PrintChar 
		add  $2, %sp 
		add $1, %bx 
		jmp .MainLoop 		
	.printdone: 
		FunctionExit 
		ret 


PrintHex: 
	FunctionEntry 
	pushw $PrintHexPreStr 
	call PrintString 
	add $2, %sp 
	movw (%bp), %cx 
	movw $4, %ax 
	movw $HexStr, %di 
	.TheLoopyLoop: 
		cmp $0, %ax 
		je .itisdone 
		rol $4, %cx 
		movw %cx, %bx 
		and $0x0F, %bx 
		add %di, %bx 
		movw (%bx), %bx 
		pushw %bx 
		call PrintChar 
		add $2, %sp 
		dec %ax 
		jmp .TheLoopyLoop  
	.itisdone: 
		FunctionExit 
		ret 		
	
HexStr: 
	.ascii "0123456789ABCDEF" 
	
PrintHexPreStr: 
	.ascii "0x"  
	.byte 0x5E 

PrintMenu: 
	FunctionEntry 
	movw $video_cursor_column, %di 
	movw $video_cursor_row, %si  
	movb $0x07, %dl 
	movb $0x07, %dh 
	movb %dl, (%di) 
	movb %dl, (%si) 
	call video_updatecursor 
	movw $0x6000, %bx  
	xor %cx, %cx  
	.MenuLoop: 
		movb (%bx), %al 
		cmp $0x3B, %al  
		je .printdone2 
		cmp $0x5E, %al 
		je .AdvanceARow
		pushw %ax 
		call PrintChar
		add  $2, %sp 
		inc %bx 
		jmp .MenuLoop 
		.AdvanceARow: 
			add $1, %dh 
			movb %dh, (%si) 
			movb %dl, (%di) 
			call video_updatecursor 
			inc %bx 
			inc %cx 
			jmp .MenuLoop 	
	.printdone2: 
		movw $EntriesNum, %di 
		movb %cl, (%di) 
		FunctionExit 
		ret

EntriesNum: 
	.byte 0
		

video_hidecursor: 
	pusha 
	movb $0x01, %ah 
	movw $0x2607, %cx 
	int $0x10 
	popa 
	ret 

#Args (push order): Number of chars (word), Cursor position (word), Page num and color (word) 
video_changecolor: 
	FunctionEntry 
	movw 0x4(%bp), %dx 
	movw 0x2(%bp), %ax 
	movw (%bp), %bx 
	movw $video_cursor_row, %si 
	movw $video_cursor_column, %di 
	movb %ah, (%si) 
	movb %al, (%di) 
	call video_updatecursor 
	movw $1, %cx 
	.ColorLoop: 
		push %ax 
		movb $0x08, %ah 
		int $0x10 
		movb $0x09, %ah 
		int $0x10 	
		dec %dx 
		pop %ax 
		inc %al 
		movb %al, (%di) 
		call video_updatecursor 
		cmp $0, %dx 
		jne .ColorLoop 
	FunctionExit 
	ret 

video_cursor_row: 
	.byte 0 
video_cursor_column: 
	.byte 0 
 
video_updatecursor:  
	FunctionEntry 
	movw $video_cursor_row, %si 
	movb (%si), %dh 
	movw $video_cursor_column, %si 
	movb (%si), %dl 
	movb $0x02, %ah 
	movb $0, %bh 
	DoInterrupt 0x10  
	FunctionExit 
	ret 

video_advancecursor:
	FunctionEntry  
	movw $video_cursor_row, %si 
	movb (%si), %dh 
	movw $video_cursor_column, %si 
	movb (%si), %dl 
	cmpb $0x50, %dl 
	jne .add_to_column 
	.advance_to_next_row: 
		xor %ax, %ax  
		movb $0, %dl 
		addb $1, %dh 
		movb %dh, %ah 
		movb $0x50, %cl  
		divb %cl 
		movw $video_cursor_row, %si 
		movb %ah, (%si) 
		jmp .finish  
 	.add_to_column: 
	 	addb $1, %dl 
		movb %dl, (%si) 
	.finish: 
		call video_updatecursor 
		FunctionExit 
		ret 

video_init: 
	FunctionEntry
	movw $0x03, %ax 
	DoInterrupt 0x10 
	FunctionExit 
	ret 
		
TitleMessage: 
	.ascii "Alo FlashDrive OS Loader"  
	.byte 0x5E

BootOptions: 
	.ascii "Boot options: "
	.byte 0x5E
	


#####################Disk Primitives ##################################

LoadBlocks_raw: 
	FunctionEntry  #Basic initialization 
	#movw (%bp), %dx #Load the upper 16 bits of the LBA 
	movl (%bp), %eax #Load the lower 32 bits of the LBA 
	#movw $LBALocationUpper, %di #Self-explanatory 
	#movw %dx, (%di) #Load the upper 16 bits into the packet 
	movw $LBALocationLower, %di #Self-explanatory 
	movl %eax, (%di) #Load the lower 32 bits into the packet 
	movw $BufferLocation, %di #Self-explanatory 
	movw 4(%bp), %dx #Load the buffer location 
	movw %dx, (%di) #Put it into the packet 
	movw $BlockReadSize, %di #Self-explanatory 
	movw 6(%bp), %dx  #Get the block read size 
	movw %dx, (%di) #Put it into the packet 
	movw $LBAAddressPacket, %si #Set the address of the data packet 
	movb $0x42, %ah #Function selection 
	movb $0x80, %dl #Set the drive number 
	int $0x13  #Do the interrupt call 
	FunctionExit #Cleanup and exit  
	ret  

LoadBlocks: 
	FunctionEntry  #Basic initialization 
	#movw (%bp), %dx #Load the upper 16 bits of the LBA 
	movl (%bp), %eax #Load the lower 32 bits of the LBA 
	movw $0x7c1c, %di #Self-explanatory 
	addl (%di), %eax #Add the LBA start to the desired address 
	#movw $LBALocationUpper, %di #Self-explanatory 
	#movw %dx, (%di) #Load the upper 16 bits into the packet 
	movw $LBALocationLower, %di #Self-explanatory 
	movl %eax, (%di) #Load the lower 32 bits into the packet 
	movw $BufferLocation, %di #Self-explanatory 
	movw 4(%bp), %dx #Load the buffer location 
	movw %dx, (%di) #Put it into the packet 
	movw $BlockReadSize, %di #Self-explanatory 
	movw 6(%bp), %dx  #Get the block read size 
	movw %dx, (%di) #Put it into the packet 
	movw $LBAAddressPacket, %si #Set the address of the data packet 
	movb $0x42, %ah #Function selection 
	movb $0x80, %dl #Set the drive number 
	int $0x13  #Do the interrupt call 
	FunctionExit #Cleanup and exit  
	ret  

LoadCluster: 
	FunctionEntry 
	movw $0x7c00, %di 
	xor %edx, %edx #Assert EDX to zero  
	movw 16(%di), %dx  #Get number of FATs 
 	movl 36(%di), %eax #Get sectors per FAT 
	mull %edx #Put in EAX the number of sectors taken by FATs 
	movw 14(%di), %dx #Get number of reserved sectors 
	add  %edx, %eax #Get the address of the first data sector 
	movl %eax, %ecx #Put the address of the first data sector into ECX
	xor %ebx, %ebx #Clear EBX  
	movl (%bp), %eax #Get the cluster number 
	movb 13(%di), %bl #Get number of sectors per cluster 
	sub $2, %eax #Subtract cluster number by two 
	mull %ebx #multiply the number of sectors per cluster by the cluster number 
	add %eax, %ecx #Get the first sector of the cluster 
	pushw %bx #Push the number of sectors per cluster 	
	movw 4(%bp), %dx #Get the buffer address 
	pushw %dx  #Repush it 
	pushl %ecx #Push the first sector of the cluster 
	#pushw $0 #Upper 16 bits of the LBA (unused) 
	call LoadBlocks 
	add $8, %sp 
	FunctionExit 
	ret

.org 9000 #This is just to make sure the packet is aligned and not fucking with any of the code prior 
LBAAddressPacket: 
		.byte 0x10 
		.byte 0 
		BlockReadSize: 
			.word 0 
		BufferLocation: 
			.word 0 
		.word 0 
		LBALocationLower: 
			.long 0 
		LBALocationUpper: 
			.long 0 



